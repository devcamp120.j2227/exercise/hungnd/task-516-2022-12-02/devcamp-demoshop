
import productData from '../../assets/js/ProductData.json';

const Content = () => {
    const limitProduct = productData.Products.slice(0, 9);
    return (
        <div className="container">
            <h3 className="center">Product List</h3>
            <div>
                <p>Showing 1 - 9 of 24 products</p>
            </div>
            <div className="row">
                {limitProduct.map((product, index) => (
                    <div className="col-6 col-sm-4">
                        <div className="product-box card bg-light mb-3">
                            <div className="card-header">
                                <h5 className="card-title">
                                    <a href="/product-detail/1">{product.Title}</a>
                                </h5>
                            </div>
                            <div className="card-body">
                                <div className="text-center">
                                    <a href="/product-detail/1">
                                        <img className="card-img-top" alt={product.Title} src={product.ImageUrl} />
                                    </a>
                                </div>
                                <p className="card-text description">
                                    {product.Description}
                                </p>
                                <p className="card-text">
                                    <b>Category:</b>
                                    &nbsp;{product.Category}
                                </p>
                                <p className="card-text">
                                    <b>Made by:</b>
                                    &nbsp;{product.Manufacturer}
                                </p>
                                <p className="card-text">
                                    <b>Organic:</b>
                                    &nbsp;{product.Organic == true ? "Yes" : "No"}
                                </p>
                                <p className="card-text">
                                    <b>Price:</b>
                                    &nbsp;${product.Price}
                                </p>
                                <div className="add-cart-container">
                                    <button type="button" className="btn btn-primary">Add to Cart</button>
                                </div>
                            </div>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    )
}

export default Content;