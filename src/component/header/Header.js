import { Component } from "react";
import Logo from "./logo/Logo";
import Navbar from "./navbar/Navbar";

class Header extends Component {
    render() {
        return (
            <>
                <Logo/>
                <Navbar/>
            </>
        )
    }
}
export default Header;