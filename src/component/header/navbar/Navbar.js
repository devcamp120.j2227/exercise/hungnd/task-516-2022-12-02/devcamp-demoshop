import { Component } from "react";

class Navbar extends Component {
    render() {
        return (
            <>
                <nav className="navbar navbar-expand-md bg-dark navbar-dark">
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar"><span className="navbar-toggler-icon"></span></button>
                    <div className="collapse navbar-collapse" id="collapsibleNavbar">
                        <ul className="navbar-nav mr-auto">
                            <li className="nav-item">
                                <a aria-current="page" className="nav-link active" href="/">Home</a>
                            </li>
                        </ul>
                    </div>
                    <div></div>
                </nav>
            </>
        )
    }
}
export default Navbar;