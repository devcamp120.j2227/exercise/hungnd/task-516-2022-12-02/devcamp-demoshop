import { Component } from "react";
import logoReact from '../../../assets/images/logo.svg';

class Logo extends Component {
    render() {
        return(
            <>
                <header>
                    <div className="container text-center">
                        <div className="logo">
                            <a href="/">
                                <img src={logoReact} alt="React Store" />
                            </a>
                        </div>
                        <h1 style={{height: "62px"}}>React Store</h1>
                        <p>Demo App Shop24h v1.0</p>
                    </div>
                </header>
            </>
        )
    }
}
export default Logo;