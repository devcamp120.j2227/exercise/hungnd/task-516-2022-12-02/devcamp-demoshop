import { Component } from "react";

class Inner extends Component {
    render() {
        return (
            <>
                <div className="row no-gutters social-container">
                    <div className="col">
                        <a className="social-inner" href="#facebook">
                            <span className="icon mdi mdi-facebook"></span>
                            <span>Facebook</span>
                        </a>
                    </div>
                    <div className="col">
                        <a className="social-inner" href="#instagram">
                            <span className="icon mdi mdi-instagram"></span>
                            <span>instagram</span>
                        </a>
                    </div>
                    <div className="col">
                        <a className="social-inner" href="#twitter">
                            <span className="icon mdi mdi-twitter"></span>
                            <span>twitter</span>
                        </a>
                    </div>
                    <div className="col">
                        <a className="social-inner" href="#google">
                            <span className="icon mdi mdi-youtube-play">
                            </span>
                            <span>google</span>
                        </a>
                    </div>
                </div>
            </>
        )
    }
}
export default Inner;