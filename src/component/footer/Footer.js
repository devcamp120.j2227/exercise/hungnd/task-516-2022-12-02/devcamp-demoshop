import { Component } from "react";
import Info from "./info/Info";
import Inner from "./inner/Inner";

class Footer extends Component {
    render() {
        return (
            <>
                <footer className="section footer-classic context-dark bg-image">
                    <Info/>
                    <Inner/>
                </footer>
            </>
        )
    }
}
export default Footer;