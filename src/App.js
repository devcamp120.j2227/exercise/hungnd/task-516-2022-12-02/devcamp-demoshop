import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Content from './component/content/Content';
import Footer from './component/footer/Footer';
import Header from './component/header/Header';

function App() {
  return (
    <div>
      <Header/>
      <Content/>
      <Footer/>
    </div>
  );
}

export default App;
